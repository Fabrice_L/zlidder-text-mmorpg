# ZLIDDER TEXT MMORPG #

In this repository, you'll be able to find my progress on a textbased MMORPG called Zlidder.

## Features ##
* Real-time chat with other players
* Npcs
* Movement through rooms
* Player Stats
* Player equipment
* Game Objects

## To-Do ##
* Skilling
* Combat (PVN/PVP)
* Trading
* Private chat (friends)
* Quests
* Bosses

If you have any questions, feel free to contact me on skype (mod.page)

## Contributors ##
* Fabrice L