function getObjects(packetData) {
	text = "";
	if (packetData[0] != null) {
		text += "</br><div>";
		text += "There are useful objects in your surroundings:";
		text += "<ul>";
		for (i = 0; i < packetData.length; i++) {
			text += "<li>" + packetData[i][0]  + getObjectCommands(i, packetData[i][1]) + "</li>";
		}
		text += "</ul></div>";
		text += "</br><div>";
	}
	return text;
}

function getObjectCommands(index, packetData) {
	text = "";
	if (packetData[0] != null ) {
		text += ": ";
		console.log(packetData);
		for (cmd_index = 0; cmd_index < packetData.length; cmd_index++) {
			text += "&lt;<a id='object-" + index + "_cmd-" + cmd_index + "'>" + packetData[cmd_index] + "</a>&gt; ";
		}
	}	
	return text;
}

function enableObjectCommands(packetData) {		
	for (let index = 0; index < packetData.length; index++) {
		for (let cmd = 0; cmd < packetData[index][1].length; cmd++) {
			var string = "object-" + index + "_cmd-" + cmd;
			
			id(string).onclick = function(e){
				console.log("Clicked object_index: " + index + ", command_index: " + cmd);
				sendPacket(JSON.stringify([8, index, cmd]));
			} 
			
			id(string).style.cursor = 'pointer';
			id(string).style.textDecoration='underline';
			
			id(string).onmouseover = function() {
				this.style.color = 'red';
				this.style.textDecoration='';
			}
			
			id(string).onmouseout = function() {
				this.style.color = '';
				this.style.textDecoration='underline';
			}		
		}
	}
}