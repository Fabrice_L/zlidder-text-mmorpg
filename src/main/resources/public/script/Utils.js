window.addEventListener("beforeunload", function (e) {
	if (!forceClose) {
		var confirmationMessage = "Refreshing the page will take you back to the loginscreen";
		e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
		return confirmationMessage;              // Gecko, WebKit, Chrome <34
	}
	return true;
});
function id(id) {
    return document.getElementById(id);
}

function GetElementInsideContainer(containerID, childID) {
    var elm = document.getElementById(childID);
    var parent = elm ? elm.parentNode : {};
    return (parent.id && parent.id === containerID) ? elm : {};
}

function integerToKOrMillion(int) {
	var s = int.toString();
	if (s.length > 7) {
		return s.substring(0, s.length - 6) + "M";
	} else if (s.length > 4) {
		return s.substring(0, s.length - 3) + "K";
	} else {
		return s;
	}
}
