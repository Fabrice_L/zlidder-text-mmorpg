function updateStat(statId, statLevel, statMax, statExperience) {
	playerLevel[statId] = statLevel;
	maxLevel[statId] = statMax;
	playerExperience[statId] = statExperience;
		
	if (id("statlist") != null) {
		id("statlist").getElementsByTagName("leveldata")[statId].innerHTML=playerLevel[statId] + '/' + maxLevel[statId];
	}
	
	if (id("currenthp") != null && statId == 3) {
		id("currenthp").innerHTML='<div class="progress-bar progress-bar-striped progress-bar-success" role="progressbar" style="width: ' + (playerLevel[statId] /  maxLevel[statId]) * 100 + '%" aria-valuenow="'+ playerLevel[statId] +'" aria-valuemin="0" aria-valuemax="'+ maxLevel[statId] +'">' +
	                    'HP:&nbsp;' + playerLevel[statId] + '/' + maxLevel[statId] +
	                '</div>';
	}
}

function drawStatList() {

	id("stats").style.color = 'red';
	id("stats").style.fontWeight= 'bold';
	id("inventory").style.color = '';
	id("inventory").style.fontWeight='';
	id("equipment").style.color = '';
	id("equipment").style.fontWeight= '';

	id("sideTabContent").innerHTML= '<div id="statlist">' +
		getStats() +
		'</div>';
}	

function getStats() {
	text="";
	for (i = 0; i < skillName.length; i++) { 
   		text += '<div>' + skillName[i] + ': <leveldata>' + playerLevel[i] + '/' + maxLevel[i] + '</leveldata></div>';
	}
	return text;
}