function drawPvNScreen(packetData) {

	playerName = packetData[0];
	npcName = packetData[1];
	npcLevel = packetData[2];
	npcAttackStyles = packetData[3];
	npcHitpoints = packetData[4];
	npcMaxHitpoints = packetData[5];

	id("contentBox").innerHTML='<div class="col col-md-6">' +
    '<div class="panel panel-warning">' +
        '<div class="panel-heading">' + playerName + '</div>' +
	        '<div class="panel-body">' +       
	            '<div class="progress"  id ="currenthp" >' +
	                '<div class="progress-bar progress-bar-striped progress-bar-success" role="progressbar" style="width: ' + (playerLevel[3] /  maxLevel[3]) * 100 + '%">' +
	                    'HP:&nbsp;' + playerLevel[3] + '/' + maxLevel[3] +
	                '</div>' +
				'</div>' +          
	        '</div>' +
    	'</div>' +
	'</div>' +
	'<div class="col col-md-6">' +
        '<div class="panel panel-danger">' +
        '<div class="panel-heading">' + npcName + '</div>' +
        '<div class="panel-body">' +
                '<div class="progress" id="npcHpBar">' +
                '<div class="progress-bar progress-bar-striped progress-bar-success" role="progressbar" style="width:'+ (npcHitpoints / npcMaxHitpoints)*100 +'%">' +
                    'HP:&nbsp;' + npcHitpoints + '/' + npcMaxHitpoints +
                '</div></div>' +
                '<div id="npcData"><div><strong>Level: </strong>' + npcLevel + '</div>' +
                '<div><strong>AttackStyles: </strong>' + npcAttackStyles + '</div></div>' +
    '</div></div>' +
'</div>' +
    '<center><div id="combatButton"><button type="submit" class="btn btn-success" id="button_1">Fight</button></div></center><br>' +
    '<div class="col col-md-12">' +
        '<div class="panel panel-default">' +
        '<div class="panel-heading">Combat Info</div>' +
        '<div class="panel-body" style="height:135px; overflow:auto" id="combatInfo"></div>' +
    '</div>' +
    '</div>' ;
    
	clickButton();
	id("combatInfo").style.fontSize="small";
}


function updatePvNScreen(packetData) {
	var updateType = packetData[0];
	
	if (updateType == "NPC_INFO") {
		var currentNpcHp = packetData[1];
		var maxNpcHp = packetData[2];
		if (id("npcHpBar") != null) {
			id("npcHpBar").innerHTML=
			'<div class="progress-bar progress-bar-striped progress-bar-success" role="progressbar" style="width:'+ (currentNpcHp / maxNpcHp)*100 +'%">' +
	           'HP:&nbsp;' + currentNpcHp + '/' + maxNpcHp +
	        '</div>';
		}
	}
	
	if (updateType == "COMBAT_INFO_NPC") {
		var damage = packetData[1];
		var anim = packetData[2];
		var out = document.getElementById("combatInfo");
		var isScrolledToBottom = out.scrollHeight - out.clientHeight <= out.scrollTop + 1;
		var newElement = document.createElement("div");
    	newElement.id = "combatInfoLine";
    	newElement.innerHTML = '<font color="red">Your enemy�s ' + anim + ' (' + damage + ' damage)</font>';
    	out.appendChild(newElement);
    	
    	
    	if(isScrolledToBottom) {
      		out.scrollTop = out.scrollHeight - out.clientHeight;
		} 
		
		if (out.getElementsByTagName("div").length > 100) {
			$('#combatInfo').find('div:first').remove();
		}
    	
	}
	if (updateType == "COMBAT_INFO_USER") {
		var damage = packetData[1];
		var anim = packetData[2];
		var out = document.getElementById("combatInfo");
		var isScrolledToBottom = out.scrollHeight - out.clientHeight <= out.scrollTop + 1;
		var newElement = document.createElement("div");
    	newElement.id = "combatInfoLine";
    	newElement.innerHTML = '<font color="orange">Your ' + anim + ' (' + damage + ' damage)</font>';
    	out.appendChild(newElement);
    	
    	
    	if(isScrolledToBottom) {
      		out.scrollTop = out.scrollHeight - out.clientHeight;
		} 
		
		if (out.getElementsByTagName("div").length > 100) {
			$('#combatInfo').find('div:first').remove();
		}
	}
	
	if (updateType == "WIN") {
		var lootData = packetData.slice(1);
		if (id("npcData") != null) {
			id("npcData").innerHTML=
			'<div><strong>Select your Loot:</strong><br>' +
			getLootData(lootData);
		}
		
		var out = document.getElementById("combatInfo");
		var isScrolledToBottom = out.scrollHeight - out.clientHeight <= out.scrollTop + 1;
		var newElement = document.createElement("div");
    	newElement.id = "combatInfoLine";
    	newElement.innerHTML = 'Congratulations, you won this fight!';
    	out.appendChild(newElement);
    	
    	if(isScrolledToBottom) {
      		out.scrollTop = out.scrollHeight - out.clientHeight;
		} 
		
		if (out.getElementsByTagName("div").length > 100) {
			$('#combatInfo').find('div:first').remove();
		}
		
		for (let index = 0; index < lootData.length; index++) {
			var string = "loot-" + index;
			id(string).onclick = function(e){
				sendPacket(JSON.stringify([4, "loot", index]));
			} 
			
			id(string).style.cursor = 'pointer';
			
			id(string).onmouseover = function() {
				this.style.fontWeight = 'bold';
				this.style.textDecoration='underline';
			}
			id(string).onmouseout = function() {
				this.style.fontWeight = '';
				this.style.textDecoration='';
			}
		}
		
		if (id("combatButton") != null) {
			id("combatButton").innerHTML=
				'<button type="submit" class="btn btn-success" id="button_3">Leave Fight</button>' +
				'<button type="submit" class="btn btn-warning" id="button_2">Fight Again</button>';
		}
		
		clickButton();
	}
	
	if (updateType == "COMBAT_BUTTONS") {
		if (id("combatButton") != null) {
			id("combatButton").innerHTML=
			'<button type="submit" class="btn btn-danger" id="button_1">Flee</button>';
		}
		
		clickButton();
	}
}

function getLootData(lootData) {
	text="";
	for (i = 0; i < lootData.length; i++) { 
		var itemName = lootData[i][0];
		var itemAmount = lootData[i][1];
   		text += '<div style="width:100%" class="btn btn-default btn-sm" id="loot-' + i + '">';
   		if (itemName != null) {	
   			text += '<div class="pull-left">' + itemName + '</div>';
   		}
   		if (itemAmount > 1) {
   			text += ' <div class="badge pull-right">' + integerToKOrMillion(itemAmount) + '</div>';
   		}
   		text += '</div><br>';
	}	
	return text;
}