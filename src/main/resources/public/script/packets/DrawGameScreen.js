function drawGameScreen() {		
	id("main").innerHTML=
	
	'<div class="row"  style="margin-top: 5%;">' +
		'<div class="col col-md-3">' +
			'<div class="text-center panel panel-default">' +
				'<div class="panel-heading">' +
	    			'<h3 class="panel-title"><i class="fa fa-users"></i> Players Here:</strong> (<span id="playeramount"></span>)</h3>' +
	  			'</div>' +
	  			'<div class="panel-body" id="playerlist"></div>' +	  		
			'</div>' +
		'</div>' +
		
		'<div class="col col-md-6">' +
			'<div class="panel panel-default">' +
				'<div class="panel-heading">' +
	    			'<h3 class="panel-title" id="headerText"></strong></h3>' +
	  			'</div>' +
	  			'<div class="panel-body" id="contentBox"></div>' +	
			'</div>' +
			'<div class="panel panel-default">' +
				'<div class="panel-heading">' +
	    			'<h3 class="panel-title"><i class="fa fa-commenting"></i> Chat</strong></h3>' +
	  			'</div>' +
	  			'<div class="panel-body"  style="height:135px; overflow:auto" id="chatbox">' +
	  				'<div id="chatmessage"><strong>Welcome To Zlidder.</strong>' +
	  				'</div>' +
	  			'</div>' +
				'<div class="panel-footer"><input type="text" id="messagebody" style="width: 100%;" placeholder="Type Enter or click here to send message"></div>' +
			'</div>' +
		'</div>' +
		
		'<div class="col col-md-3">' +
			'<div class="panel panel-default">' +
				'<div class="panel-heading">' +
	    			'<h3 class="panel-title"><i class="fa fa-compass"></i> Locations</strong></h3>' +
	  			'</div>' +
	  			'<div class="panel-body" id="locationBox"></div>' +	  		
			'</div>' +
			'<div class="panel panel-default">' +
				'<div class="panel-heading" id="sidetabs">' +
	    			'<div class="btn-group btn-group-justified">' +
                    	'<div id="stats" class="btn btn-lg btn-default"><i class="fa fa-bar-chart" aria-hidden="true"></i></div>' +
                    	'<div id="inventory" class="btn btn-lg btn-default"><i class="fa fa-suitcase" aria-hidden="true"></i></div>' +
                    	'<div id="equipment" class="btn btn-lg btn-default"><i class="fa fa-user" aria-hidden="true"></i></div>' +
            		'</div>' +
	  			'</div>' +
	  			'<div class="panel-body" style="min-height: 299; max-height: 299; overflow:auto;" id="sideTabContent"></div>' +
				'<div class="panel-footer">' +
					'<div class="btn-group btn-group-justified">' +
                    	'<div id="empty" class="btn btn-default btn-lg disabled"><i class="fa fa-smile-o" aria-hidden="true"></i></div>' +
                    	'<div id="empty" class="btn btn-default btn-lg disabled"><i class="fa fa-frown-o" aria-hidden="true"></i></div>' +
                    	'<div id="empty" class="btn btn-default btn-lg disabled"><i class="fa fa-cogs" aria-hidden="true"></i></div>' +
            			'<div id="empty" class="btn btn-default btn-lg disabled"><i class="fa fa-sign-out" aria-hidden="true"></i></div>' +
            		'</div>' +
				'</div>' +
			'</div>' +
		'</div>' +
	'</div>';	

	$.getScript("./script/packets/SendChat.js", function(){
	});
	$.getScript("./script/packets/KeyShortCuts.js", function() {
	});
			
	id("stats").onclick = function() {
		drawStatList();
	};
	
	id("inventory").onclick = function() {
		drawInventoryContainer();
	};
	
	id("equipment").onclick = function() {
		drawEquipmentContainer();
	};
	
	id("inventory").click();
	
	id("stats").style.cursor = 'pointer';
	id("inventory").style.cursor = 'pointer';
	id("equipment").style.cursor = 'pointer';
	
	id("sidetabs").style.fontSize="small";
	id("chatbox").style.fontSize="small";
	id("locationBox").style.fontSize="small";
	id("sideTabContent").style.fontSize="small";
	
}

