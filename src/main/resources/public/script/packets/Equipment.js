function drawEquipmentContainer() {
	id("stats").style.color = '';
	id("stats").style.fontWeight= '';
	id("inventory").style.color = '';
	id("inventory").style.fontWeight='';
	id("equipment").style.fontWeight='bold';
	id("equipment").style.color='red';
		
	id("sideTabContent").innerHTML= '<div id="equipmentContainer">' +
	'<div><strong>Head:</strong> ' + drawEquipmentSlot(0) + '</div>' +
	'<div><strong>Neck:</strong> ' + drawEquipmentSlot(1) + '</div>' +
	'<div><strong>Weapon:</strong> ' + drawEquipmentSlot(2) + '</div>' +
	'<div><strong>Quiver:</strong> ' + drawEquipmentSlot(3) + '</div>' +
	'<div><strong>Shield:</strong> ' + drawEquipmentSlot(4) + '</div>' +
	'<div><strong>Chest:</strong> ' + drawEquipmentSlot(5) + '</div>' +
	'<div><strong>Cape:</strong> ' + drawEquipmentSlot(6) + '</div>' +
	'<div><strong>Legs:</strong> ' + drawEquipmentSlot(7) + '</div>' +
	'<div><strong>Feet:</strong> ' + drawEquipmentSlot(8) + '</div>' +
	'</div>';
	
	for (let index = 0; index < equipment.length; index++) {
		var string = "equipment-" + index;
		id(string).onclick = function(e){
			sendPacket(JSON.stringify([4, "equipment", index]));
		} 
		
		id(string).style.cursor = 'pointer';
		
		id(string).onmouseover = function() {
			this.style.fontWeight = 'bold';
			this.style.textDecoration='underline';
		}
		id(string).onmouseout = function() {
			this.style.fontWeight = '';
			this.style.textDecoration='';
		}
	}
}

function updateEquipmentContainer() {
	if (id("equipmentContainer") != null) {
		drawEquipmentContainer();
	}
}

function drawEquipmentSlot(slot) {
	text = "";
	if (slot in equipment) {
		var itemName = equipment[slot][0];
		var itemAmount = equipment[slot][1];
		text += '<equipment id="equipment-' + slot + '">';
		if (itemName != null) {
			text += itemName + ' (' + integerToKOrMillion(itemAmount) + ')';
		}
		text += '</equipment>';
	}
	return text;
}
