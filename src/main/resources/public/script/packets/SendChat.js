id("messagebody").addEventListener("keypress", function (e) {
    if (e.keyCode === 13 && id("messagebody").value != "") {
		sendPacket(JSON.stringify([2, id("messagebody").value]));
		id("messagebody").value = "";    
	}
});

var sendmessage = "";

function addChatMessage(username, message) {
	sendmessage = username + ": " + message;
	updateScroll();
}
function addServerMessage(message) {
	sendmessage = "<strong>" + message + "</strong>";
	updateScroll();
}

function updateScroll() {
	var out = document.getElementById("chatbox");
    var isScrolledToBottom = out.scrollHeight - out.clientHeight <= out.scrollTop + 1;
    var newElement = document.createElement("div");
    newElement.id = "chatmessage";
    newElement.innerHTML = sendmessage;
    out.appendChild(newElement);

    if(isScrolledToBottom) {
      out.scrollTop = out.scrollHeight - out.clientHeight;
	}      

	if (out.getElementsByTagName("div").length > 100) {
		$('#chatbox').find('div:first').remove();
	}
}


