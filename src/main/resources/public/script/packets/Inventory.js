function drawInventoryContainer() {
	id("stats").style.color = '';
	id("stats").style.fontWeight= '';
	id("equipment").style.color = '';
	id("equipment").style.fontWeight= '';
	id("inventory").style.color = 'red';
	id("inventory").style.fontWeight='bold';
		
	id("sideTabContent").innerHTML= 
	  	'<div class="form-group">'+
     '<select class="form-control" id="selectAction">'+
        '<option>Use/Equip</option>'+
        '<option>Use With</option>'+
        '<option>Drop</option>'+
      '</select>'+
    '</div>'+
	
	
		'<div id="inventoryContainer">' +
		getInvItems(invItems) +
	'</div>';
	
	clickInventoryStuff();
}

function getInvItems(itemData) {
	text="";
	for (i = 0; i < itemData.length; i++) {
		var itemName = itemData[i][0];
		var itemAmount = itemData[i][1];
   		if (itemName != null) {	
   		   	text += '<div style="width:100%" class="btn btn-default btn-sm" id="inventory-' + i + '">';
   			text += '<div class="pull-left"> ' + itemName + '</div>';
   			if (itemAmount > 1) {
   				text += ' <div class="badge pull-right">' + integerToKOrMillion(itemAmount) + '</div>';
   			}
   			text += '</div><br>';
   		}
	}	
	return text;
}

function updateInventory() {
		
	if (id("inventoryContainer") != null) {
		id("inventoryContainer").innerHTML = getInvItems(invItems);
	}
	clickInventoryStuff();
	
}

function clickInventoryStuff() {
	for (let index = 0; index < invItems.length; index++) {
		var string = "inventory-" + index;
		if (id(string) != null) {
			id(string).onclick = function(e){
				var itemAction = "USE";
				var menuAction = id("selectAction").value;
				if (menuAction == "Drop") {
					itemAction = "DROP";
				}
				if (menuAction == "Use With") {
					itemAction = "USEWITH";
				}
				sendPacket(JSON.stringify([4, "inventory", index, itemAction]));
			} 
			
			id(string).style.cursor = 'pointer';
			
			id(string).onmouseover = function() {
				this.style.fontWeight = 'bold';
				this.style.textDecoration='underline';
			}
			id(string).onmouseout = function() {
				this.style.fontWeight = '';
				this.style.textDecoration='';
			}
		}
	}
}
