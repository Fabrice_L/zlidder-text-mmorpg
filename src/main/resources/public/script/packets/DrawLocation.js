var directions = [];
function drawLocation(packetData) {
	directions = [];
	var text = "";
	for (i = 0; i < packetData.length; i++) {
		directions.push(packetData[i][0]);
		var direction = packetData[i][0];
		var roomName = packetData[i][1];
		text += "<div><a href='#' id='" + direction + "'>" + direction + ": " + roomName + "</a></div>";
	}
	id("locationBox").innerHTML= text;
	
	for (int = 0; int < directions.length; int++) {
		id(directions[int]).onclick = function(e){
			e.preventDefault();
			sendPacket(JSON.stringify([6, this.id]));
		} 
	}
}