window.onload = function() {
  id("username").focus();
};

id("login").addEventListener("click", function () {
    sendPacket(JSON.stringify([1, id("username").value, id("password").value]));
});

id("username").addEventListener("keypress", function (e) {
	if (e.keyCode == 13) {
		id("password").focus();
	}
});

id("password").addEventListener("keypress", function (e) {
	if (e.keyCode == 13) {
		id("login").click();
	}
});

function doLogin(returnCode) {
	if (returnCode == 2) {
		drawGameScreen();
	}
	if (returnCode == 3) {
		response = "Wrong username/password";
		id("loginresponse").innerHTML=response;
	}
	
	if (returnCode == 5) {
		response = "Already logged in";
		id("loginresponse").innerHTML=response;
	}
}