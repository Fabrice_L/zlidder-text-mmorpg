var map = {}; // You could also use an array
onkeydown = function(e){
    e = e || event; // to deal with IE
    map[e.keyCode] = e.type == 'keydown';
    keyCodes = [37, 38, 39, 40, 9];
    for (i = 0; i < keyCodes.length; i++) {
    	if (keyCodes[i] == e.keyCode) {
    		e.preventDefault();
    	}
    }
    if (map[17] && map[73]) {
		e.preventDefault();
	} else if (map[17] && map[83]) {
		e.preventDefault();
	}  else if (map[17] && map[69]) {
		e.preventDefault();
	}
}
onkeyup = function(e) {
	/* public chat */
	if (map[13]) {
		id("messagebody").focus();
	}
	/* SideTabs by keycombo */
	if (map[17] && map[73]) {
		id("inventory").click();
	} else if (map[17] && map[83]) {
		id("stats").click();
	} else if (map[17] && map[69]) {
		id("equipment").click();
	}
	/* Movement by arrowkeys */
    if(map[38] && map[39] || map[39] && map[38]){ // NorthEast
    	sendPacket(JSON.stringify([6, "NorthEast"]));
	} else if (map[37] && map[38] || map[38] && map[37]) { // NorthWest
		sendPacket(JSON.stringify([6, "NorthWest"]));
	} else if (map[40] && map[39] || map[39] && map[40]) { // SouthEast
		sendPacket(JSON.stringify([6, "SouthEast"]));
	} else if (map[40] && map[37] || map[37] && map[40]) { // SouthWest
		sendPacket(JSON.stringify([6, "SouthWest"]));
	} else if (map[40]) { // South
		sendPacket(JSON.stringify([6, "South"]));
	} else if (map[38]) { // North
		sendPacket(JSON.stringify([6, "North"]));
	} else if (map[37]) { // West
		sendPacket(JSON.stringify([6, "West"]));
	} else if (map[39]) { // East
		sendPacket(JSON.stringify([6, "East"]));
	}
	map = {};
}