var locationName = "";
var roomName = "";

function drawRoom(packetData) {
	locationName = packetData[0];
	roomName = packetData[1];
	roomDescription = packetData[2];
	npcs = packetData[3];
	objects = packetData[4];
	
	id("headerText").innerHTML="<div><i class='fa fa-home' aria-hidden='true'></i> " + locationName + "</div>";
	id("contentBox").innerHTML="<div>" + roomDescription + "</div>" 
		+ getNpcs(npcs)
		+ getObjects(objects)
	;
	
	enableNpcCommands(npcs);
	enableObjectCommands(objects);

}

