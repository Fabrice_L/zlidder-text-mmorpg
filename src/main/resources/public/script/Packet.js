var forceClose = false;
var webSocket = new WebSocket("ws://" + location.hostname + ":" + location.port + "/game/");
webSocket.onmessage = function (msg) { handlePacket(msg); };
webSocket.onclose = function () { forceClose = true; window.location.reload();};


function sendPacket(message) {
    if (message !== "") {
        webSocket.send(message);
    }
}

function handlePacket(message) {
	var packetData = JSON.parse(message.data);	
	var packetId = packetData[0];
	console.log(packetData);
	
	if (packetId == 1) {
		var returnCode = packetData[1];
				console.log("code = " + returnCode);
		
		doLogin(returnCode);
	}
	
	if (packetId == 2) {
		drawPlayersOnLocation(packetData[1]);
	}	
	
	if (packetId == 3) {
		var type = packetData[1];
		if (type == 'CHAT_MESSAGE') {
			var username = packetData[2];
			var message = packetData[3];
			addChatMessage(username, message);
		}
		if (type == 'SERVER_MESSAGE') {
			var message = packetData[2];
			addServerMessage(message);
		}
	}
	
	if (packetId == 4) {
		var statId = packetData[1];
		var statLevel = packetData[2];
		var statMax = packetData[3];
		var statExperience = packetData[4];
		playerLevel[statId] = statLevel;
		maxLevel[statId] = statMax;
		playerExperience[statId] = statExperience;
		updateStat(statId, statLevel, statMax, statExperience);
	}
	
	if (packetId == 5) {
		var itemContainer = packetData[1];
		var itemData = packetData.slice(2);
		console.log(itemContainer);
		if (itemContainer == "INVENTORY") {
			invItems = itemData;
			updateInventory();
		}
		if (itemContainer == "EQUIPMENT") {
			equipment = itemData;
			updateEquipmentContainer();
			console.log(itemData);
		}
	}
	
	if (packetId == 6) {
		var roomData = packetData.slice(1);
		drawRoom(roomData);
	}
	
	if (packetId == 7) {
		var locationData = packetData.slice(1);
		drawLocation(locationData);
	}
	
	if (packetId == 8) {
		console.log("trying this");
		drawPvNScreen(packetData.slice(1));
	}
	
	if (packetId == 9) {
		updatePvNScreen(packetData.slice(1));
	}
}