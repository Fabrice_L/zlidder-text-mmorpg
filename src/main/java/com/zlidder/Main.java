package com.zlidder;

import static spark.Spark.init;
import static spark.Spark.port;
import static spark.Spark.staticFileLocation;
import static spark.Spark.webSocket;

import java.io.File;

import org.quartz.SchedulerException;

import com.zlidder.jobs.JobScheduler;
import com.zlidder.model.item.ItemDefinition;
import com.zlidder.model.npc.NpcDefinition;
import com.zlidder.model.npc.NpcDrop;
import com.zlidder.net.WebSocketHandler;

public class Main {

	public static void main(String[] args) {
		
		ItemDefinition.loadItems(new File("data/items.json"));
		NpcDefinition.loadNpcs(new File("data/npcs.json"));
		NpcDrop.loadNpcDrops(new File("data/npcDrops.json"));
		
		port(80);
        staticFileLocation("/public");
        
        webSocket("/game", WebSocketHandler.class);
        init();

        try {
			new JobScheduler();
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
