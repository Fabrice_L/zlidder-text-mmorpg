package com.zlidder.jobs;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;



/**
 * Created by fabrice on 6-6-2015.
 */

public class JobScheduler {

	private static Scheduler scheduler;
	
    public JobScheduler() throws SchedulerException{
        scheduler = new StdSchedulerFactory("quartz.properties").getScheduler();   
        scheduler.start();
    }
    
    public static void schedule(int milliSeconds, Class<? extends Job> object, Object constructor) {

        JobDetail job = JobBuilder.newJob(object).build();
        
        job.getJobDataMap().put("action", constructor);
        
        
        
        Trigger trigger = TriggerBuilder.newTrigger().withSchedule(
        		SimpleScheduleBuilder
        		.simpleSchedule()
        		.withIntervalInMilliseconds(milliSeconds)
        		.repeatForever()).build();
        
        try {
			scheduler.scheduleJob(job, trigger);
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }
    public static Scheduler getScheduler() {
    	return scheduler;
    }
}
