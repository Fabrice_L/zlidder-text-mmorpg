package com.zlidder.jobs.impl;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import com.zlidder.model.combat.PvN;
import com.zlidder.model.npc.Npc;
import com.zlidder.model.user.User;

@DisallowConcurrentExecution
public class PvNJob implements Job {

	private JobExecutionContext context;
	private User user;
	private Npc npc;
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		this.context = context;
		PvN pvn = (PvN) context.getJobDetail().getJobDataMap().get("action");
		user = pvn.getUser();
		npc = pvn.getNpc();

		if (!user.isOnline() || npc == null || !user.isInCombat()) {
			pvn.resetCombat(true, false);
			stop();
			return;
		}
		
		if (user.getAttackTimer() <= 0) {
			pvn.playerDamage();
		}
		user.setAttackTimer(user.getAttackTimer() - 1);
		
		if (npc.getCurrentHitpoints() <= 0) {
			pvn.resetCombat(false, false);
			stop();
			return;
		}
		
		if (npc.getAttackTimer() <= 0) {
			pvn.npcDamage();
		}	
		npc.setAttackTimer(npc.getAttackTimer() - 1);
		
		if (user.getStats().getPlayerLevels().get(3) <= 0) {
			pvn.resetCombat(false, true);
			stop();
			return;
		}

		
	}

	
	
	private void stop() {
		try {
			context.getScheduler().deleteJob(context.getJobDetail().getKey());
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

}
