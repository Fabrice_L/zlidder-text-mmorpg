package com.zlidder.jobs.impl;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import com.zlidder.model.MessageType;
import com.zlidder.model.user.content.skills.HarvestAction;
import com.zlidder.net.packet.out.MessagePacket;

@DisallowConcurrentExecution
public class HarvestJob implements Job {

	JobExecutionContext context;
	int runs = 5;
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		this.context = context;
		HarvestAction action = (HarvestAction) context.getJobDetail().getJobDataMap().get("action");
		runs --;
		if (!action.getUser().isOnline() || runs == 0) {
			stop();
			return;
		}
		action.getUser().send(new MessagePacket("running harvest job for: " + action.getUser().getUsername() + " (" + runs + " runs left)", MessageType.SERVER_MESSAGE));
	}
	
	private void stop() {
		try {
			context.getScheduler().deleteJob(context.getJobDetail().getKey());
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

}
