package com.zlidder.model.npc;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class NpcDefinition {

	Npc npc;
	static ArrayList<NpcDefinition> definitions = new ArrayList<NpcDefinition>();
	
	private int id;
	private String name;
	private int level;
	private int hitpoints;
	private int maxHit;
	private int attackTimer;
	private JSONArray animations = new JSONArray();
	private JSONArray attackStyles = new JSONArray();
	private JSONArray commands = new JSONArray();
	
	public NpcDefinition(Npc npc) {
		this.npc = npc;
	}
	
	public String getName() {
		for (NpcDefinition npcs : definitions) {
			if (npcs.id == npc.getId()) {
				return npcs.name;
			}
		}
		return null;
	}
	
	public int getLevel() {
		for (NpcDefinition npcs : definitions) {
			if (npcs.id == npc.getId()) {
				return npcs.level;
			}
		}
		return -1;
	}
	
	public JSONArray getAttackStyles() {
		for (NpcDefinition npcs : definitions) {
			if (npcs.id == npc.getId()) {
				return npcs.attackStyles;
			}
		}
		return null;
	}
	
	public JSONArray getAnimations() {
		for (NpcDefinition npcs : definitions) {
			if (npcs.id == npc.getId()) {
				return npcs.animations;
			}
		}
		return null;
	}
	
	public int getHitpoints() {
		for (NpcDefinition npcs : definitions) {
			if (npcs.id == npc.getId()) {
				return npcs.hitpoints;
			}
		}
		return -1;
	}
	
	public int getMaxHit() {
		for (NpcDefinition npcs : definitions) {
			if (npcs.id == npc.getId()) {
				return npcs.maxHit;
			}
		}
		return -1;
	}
	
	public int getAttackTimer() {
		for (NpcDefinition npcs : definitions) {
			if (npcs.id == npc.getId()) {
				return npcs.attackTimer;
			}
		}
		return -1;
	}
	
	public JSONArray getCommands() {
		for (NpcDefinition npcs : definitions) {
			if (npcs.id == npc.getId()) {
				return npcs.commands;
			}
		}
		return null;
	}
	
	public static void loadNpcs(File file) {
		try {
			JSONTokener tokener = new JSONTokener(file.toURI().toURL().openStream());
			JSONObject jsonObject = new JSONObject(tokener);
			JSONArray jsonArray = jsonObject.getJSONArray("npcs");
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject object = jsonArray.getJSONObject(i);
				NpcDefinition npc = new NpcDefinition(null);
				npc.id = object.getInt("id");
				npc.name = object.getString("name");
				npc.level = object.getInt("level");
				npc.attackStyles = object.getJSONArray("attackStyles");
				npc.animations = object.getJSONArray("animations");
				npc.hitpoints = object.getInt("hitpoints");
				npc.maxHit = object.getInt("maxHit");
				npc.attackTimer = object.getInt("attackTimer");
				npc.commands = object.getJSONArray("commands");
				definitions.add(npc);
			}
			
			System.out.println("loaded " + definitions.size() + " npc definitions.");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
