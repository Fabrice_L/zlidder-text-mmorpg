package com.zlidder.model.npc;

public class Npc {
	
	private int id;
	private int currentHitpoints = 0;
	private int damage;
	private int attackTimer;
	
	private NpcDefinition npcDefinition = new NpcDefinition(this);
	
	public Npc(int id) {
		this.id = id;
	}

	public NpcDefinition getNpcDefinitions() {
		return npcDefinition;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCurrentHitpoints() {
		return currentHitpoints;
	}

	public void setCurrentHitpoints(int currentHitpoints) {
		this.currentHitpoints = currentHitpoints;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getAttackTimer() {
		return attackTimer;
	}

	public void setAttackTimer(int attackTimer) {
		this.attackTimer = attackTimer;
	}
		
}
