package com.zlidder.model.npc;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.zlidder.model.item.GameItem;
import com.zlidder.model.item.ItemType;
import com.zlidder.util.Misc;

public class NpcDrop {

	static ArrayList<NpcDrop> dropList = new ArrayList<NpcDrop>();
	private Npc npc;
	
	private JSONArray drops;
	
	public NpcDrop(Npc npc) {
		this.npc = npc;
	}
	
	public ArrayList<GameItem> getDrops() {
		ArrayList<GameItem> drops = new ArrayList<GameItem>();
		for (NpcDrop drop : dropList) {
			if (drop.npc.getId() == npc.getId()) {
				for (int i = 0; i < drop.drops.length(); i++) {
					JSONObject object = drop.drops.getJSONObject(i);
					if (Misc.random(100) <= object.getInt("dropChance")) {
						GameItem item = new GameItem(object.getInt("itemId"), object.getInt("itemAmount"));
						drops.add(item);
					}
				}
			}
		}
		return drops;
	}

	public static void loadNpcDrops(File file) {
		try {
			JSONTokener tokener = new JSONTokener(file.toURI().toURL().openStream());
			JSONObject jsonObject = new JSONObject(tokener);
			JSONArray jsonArray = jsonObject.getJSONArray("npcDrops");
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject object = jsonArray.getJSONObject(i);
				NpcDrop drop = new NpcDrop(null);
				drop.npc = new Npc(object.getInt("npcId"));
				drop.drops = object.getJSONArray("drops");

				dropList.add(drop);
			}
			
			System.out.println("loaded " + dropList.size() + " Npc Drops.");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
