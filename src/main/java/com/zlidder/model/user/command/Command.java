package com.zlidder.model.user.command;

import org.quartz.SchedulerException;

import com.zlidder.jobs.JobScheduler;
import com.zlidder.jobs.impl.HarvestJob;
import com.zlidder.model.MessageType;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.user.Stat;
import com.zlidder.model.user.User;
import com.zlidder.model.user.content.skills.HarvestAction;
import com.zlidder.net.packet.out.MessagePacket;
import com.zlidder.net.packet.out.SetStat;
import com.zlidder.net.packet.out.UpdatePvNScreen;
import com.zlidder.net.packet.out.UpdatePvNScreen.PvNUpdateType;

public class Command {
	
	User user;
	String command;
	
	public Command(User user, String command) {
		this.user = user;
		this.command = command;
		send();
	}

	private void send() {
		String[] args = command.split(" ");	
		
		if (args[0].equalsIgnoreCase("yell")) {
			String text = command.substring(5);
			user.send(new MessagePacket(user.getUsername() + ": " + text, MessageType.SERVER_MESSAGE));
		}
		
		if (args[0].equalsIgnoreCase("level")) {
			int id = Integer.parseInt(args[1]);
			int amount = args[2] != null ? Integer.parseInt(args[2]) : 150;
			user.getStats().addExp(amount, Stat.getSkill(id));
		}
		
		if (args[0].equalsIgnoreCase("item")) {
			int id = Integer.parseInt(args[1]);
			int amount = Integer.parseInt(args[2]);
			if (id <= 10) {
				user.getInventoryContainer().addItem(new GameItem(id, amount));
			} else {
				user.send(new MessagePacket("This item does not exist!", MessageType.SERVER_MESSAGE));
			}
		}
		
		if (args[0].equalsIgnoreCase("incombat")) {
			user.send(new MessagePacket("inCombat: " + user.isInCombat(), MessageType.SERVER_MESSAGE));
		}
		
		if (args[0].equalsIgnoreCase("job")) {
			JobScheduler.schedule(1000, HarvestJob.class, 
					new HarvestAction(user));
		}
	}
}
