package com.zlidder.model.user;

import java.util.ArrayList;

import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONArray;

import com.zlidder.model.MessageType;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.item.container.impl.EquipmentContainer;
import com.zlidder.model.item.container.impl.InventoryContainer;
import com.zlidder.model.npc.Npc;
import com.zlidder.model.room.Room;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.net.packet.out.DrawPlayersOnLocation;
import com.zlidder.net.packet.out.DrawRoom;
import com.zlidder.net.packet.out.MessagePacket;
import com.zlidder.net.packet.out.SetStat;
import com.zlidder.world.UserHandler;

public class User {
	
	private Session session;
	private String username;
	private String password;
	private boolean isOnline;
	private Room room;
	private Stats stats = new Stats(this);
	private Npc inCombatWithNpc;
	private Npc lastCombatNpc;
	private boolean inCombat;
	private int attackTimer;
	private int damage;
	private GameItem itemUsed;
	private ArrayList<GameItem> loot = new ArrayList<>();
	private ArrayList<Integer> lootCollected = new ArrayList<>();
	private long lastAction = System.currentTimeMillis();
	private InventoryContainer inventoryContainer = new InventoryContainer(this);
	private EquipmentContainer equipmentContainer = new EquipmentContainer(this);
	
	public User(Session session) {
		this.session = session;
		UserHandler.getConnections().add(this);
	}
	
	public void receive(IncomingPacket packet, JSONArray packetData) {
		packet.send(this, packetData);
	}
	
	public void send(OutgoingPacket packet) {
		packet.send(this);
	}
	
	public Session getSession() {
		return session;
	}
	
	public void destroy() {
		session.close();
		username = null;
		password = null;
		isOnline = false;
		UserHandler.getUsers().remove(this);
		UserHandler.getConnections().remove(this);
		send(new DrawPlayersOnLocation(this));
	}
	
	public void initialize() {
		UserHandler.getUsers().add(this);
		send(new DrawPlayersOnLocation(this));
		for (int statId = 0; statId < getStats().getPlayerLevels().size(); statId++) {
			send(new SetStat(statId));
		}
		send(new DrawRoom(getRoom()));
	}
	
	public void initializeDeath() {
		setRoom(new Room("Serpy", 0));
		for (GameItem item : getInventoryContainer().getItems()) {
			if (item.getId() > 0) {
				getInventoryContainer().removeItem(item);
			}
		}
		getStats().getPlayerLevels().set(3, Stats.getLevelForExperience(getStats().getPlayerExperience().get(3)));
		send(new SetStat(3));
		send(new MessagePacket("Oh dear, you died!", MessageType.SERVER_MESSAGE));
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isOnline() {
		return isOnline;
	}

	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}

	public Stats getStats() {
		return stats;
	}

	public InventoryContainer getInventoryContainer() {
		return inventoryContainer;
	}
	
	public EquipmentContainer getEquipmentContainer() {
		return equipmentContainer;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Npc getInCombatWithNpc() {
		return inCombatWithNpc;
	}

	public void setInCombatWithNpc(Npc inCombatWithNpc) {
		this.inCombatWithNpc = inCombatWithNpc;
	}

	public long getLastAction() {
		return lastAction;
	}

	public void setLastAction(long lastAction) {
		this.lastAction = lastAction;
	}

	public boolean isInCombat() {
		return inCombat;
	}

	public void setInCombat(boolean inCombat) {
		this.inCombat = inCombat;
	}

	public int getAttackTimer() {
		return attackTimer;
	}

	public void setAttackTimer(int attackTimer) {
		this.attackTimer = attackTimer;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public ArrayList<GameItem> getLoot() {
		return loot;
	}

	public void setLoot(ArrayList<GameItem> loot) {
		this.loot = loot;
	}

	public ArrayList<Integer> getLootCollected() {
		return lootCollected;
	}

	public Npc getLastCombatNpc() {
		return lastCombatNpc;
	}

	public void setLastCombatNpc(Npc lastCombatNpc) {
		this.lastCombatNpc = lastCombatNpc;
	}

	public GameItem getItemUsed() {
		return itemUsed;
	}

	public void setItemUsed(GameItem itemUsed) {
		this.itemUsed = itemUsed;
	}

}
