package com.zlidder.model.user;

import java.util.ArrayList;
import java.util.List;

import com.zlidder.model.MessageType;
import com.zlidder.net.packet.out.MessagePacket;
import com.zlidder.net.packet.out.SetStat;

public class Stats {
	
	User user;
	
	public Stats(User user) {
		this.user = user;
		for (int i = 0; i <= 3; i++) {
			playerLevel.add(1);
			playerExperience.add(0);
		}
		set(Stat.HITPOINTS, 10);
	}
	
	private List<Integer> playerLevel = new ArrayList<Integer>();
	private List<Integer> playerExperience = new ArrayList<Integer>();

	public List<Integer> getPlayerLevels() {
		return playerLevel;
	}
	
	public List<Integer> getPlayerExperience() {
		return playerExperience;
	}
	
	public void addExp(int amount, Stat stat) {
		int oldLevel = getLevelForExperience(getPlayerExperience().get(stat.getId()));
		int currentHp = getPlayerLevels().get(Stat.HITPOINTS.getId());
		if (amount + getPlayerExperience().get(stat.getId()) < 0
				|| getPlayerExperience().get(stat.getId()) > 200000000) {
			user.send(new MessagePacket("EXP Limit Reached!", MessageType.SERVER_MESSAGE));
			return ;
		}
		int currentExperience = getPlayerExperience().get(stat.getId());
		getPlayerExperience().set(stat.getId(), currentExperience + amount);
		int newLevel = getLevelForExperience(getPlayerExperience().get(stat.getId()));
		if (stat.getId() != 3) {
			getPlayerLevels().set(stat.getId(), getLevelForExperience(getPlayerExperience().get(stat.getId())));
		} else {
			int levelDifference = newLevel - oldLevel;
			getPlayerLevels().set(Stat.HITPOINTS.getId(), currentHp + levelDifference);
		}
		
		if (oldLevel < newLevel) {
			user.send(new MessagePacket("You have just advanced a " + stat.getName() + " level", MessageType.SERVER_MESSAGE));
		}
		user.send(new SetStat(stat.getId()));
		return;
	}
	
	public int getExperienceForLevel(int level) {
		int points = 0;
		int output = 0;

		for (int lvl = 1; lvl <= level; lvl++) {
			points += Math.floor((double) lvl + 300.0
					* Math.pow(2.0, (double) lvl / 7.0));
			if (lvl >= level)
				return output;
			output = (int) Math.floor(points / 4) + 1;
		}
		return 0;
	}

	  public static int getLevelForExperience(int experience) {
		    int points = 0;
		    int output = 0;
		    for (int lvl = 1; lvl <= 99; lvl++) {
		      points += Math.floor((double) lvl + 300.0 * Math.pow(2.0, (double) lvl / 7.0));
		      output = (int) Math.floor(points / 4);
		      if (output >= experience) {
		        return lvl;
		      }
		    }
		    return 99;
		  }

	   public void set(Stat stat, int level) {
		   playerLevel.set(stat.getId(), level);
		   playerExperience.set(stat.getId(), getExperienceForLevel(level));
	   }
	   
}
