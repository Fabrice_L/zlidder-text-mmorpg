package com.zlidder.model.user;

public enum Stat {
	ATTACK(0, "attack"), 
	  DEFENCE(1, "defence"), 
	  STRENGTH(2, "strength"), 
	  HITPOINTS(3, "hitpoints");

	  private int id;
	  private String name;

	  Stat(int id, String name) {
	    this.id = id;
	    this.name = name;
	  }

	  public int getId() {
	    return id;
	  }

	  public String getName() {
	    return name;
	  }

	  public static Stat getSkill(int id) {
		  for (Stat stat : values()) {
			  if (stat.getId() == id)
				  return stat;
		  }
		  return null;
	  }
	  
}
