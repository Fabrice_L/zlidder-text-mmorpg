package com.zlidder.model.user.content.skills;

import com.zlidder.model.user.User;

public class HarvestAction {
	
	private User user;
	
	public HarvestAction(User user) {
		this.setUser(user);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
