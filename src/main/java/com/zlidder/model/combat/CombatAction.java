package com.zlidder.model.combat;

import com.zlidder.model.user.User;

public class CombatAction {

	private User user;
	
	public CombatAction(User user) {
		setUser(user);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
