package com.zlidder.model.combat;

import com.zlidder.jobs.JobScheduler;
import com.zlidder.jobs.impl.PvNJob;
import com.zlidder.model.MessageType;
import com.zlidder.model.item.equipment.Equipment;
import com.zlidder.model.npc.Npc;
import com.zlidder.model.npc.NpcDrop;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.out.DrawPvNScreen;
import com.zlidder.net.packet.out.DrawRoom;
import com.zlidder.net.packet.out.MessagePacket;
import com.zlidder.net.packet.out.SetStat;
import com.zlidder.net.packet.out.UpdatePvNScreen;
import com.zlidder.net.packet.out.UpdatePvNScreen.PvNUpdateType;
import com.zlidder.util.Misc;

public class PvN {

	private User user;
	private Npc npc;
	
	public PvN(User user, Npc npc) {
		this.user = user;
		this.npc = npc;
	}
	
	public void setupNpcCombat() {
		npc.setCurrentHitpoints(npc.getNpcDefinitions().getHitpoints());
		npc.setAttackTimer(npc.getNpcDefinitions().getAttackTimer());
		user.setInCombatWithNpc(npc);
		user.send(new DrawPvNScreen(npc));
		user.getLoot().clear();
		user.getLootCollected().clear();
	}
	
	public void startCombat() {
		user.setInCombat(true);
		user.send(new UpdatePvNScreen(this, PvNUpdateType.COMBAT_BUTTONS));
		JobScheduler.schedule(1000, PvNJob.class, this);
	}
	
	public void playerDamage() {
		user.setDamage(Misc.random(calculateMaxHit()));
		System.out.println("testing if this works");
		user.setAttackTimer(user.getEquipmentContainer().getItems()[Equipment.WEAPON.getSlot()].getDefinitions().getAttackTimer());
		int hp = npc.getCurrentHitpoints();
		if (user.getDamage() > hp) {
			user.setDamage(hp);
		}
		hp = hp - user.getDamage();
		
		npc.setCurrentHitpoints(hp);
		user.send(new UpdatePvNScreen(this, PvNUpdateType.NPC_INFO));
		user.send(new UpdatePvNScreen(this, PvNUpdateType.COMBAT_INFO_USER));
	}
	
	public void npcDamage() {
		npc.setDamage(Misc.random(npc.getNpcDefinitions().getMaxHit()));
		npc.setAttackTimer(npc.getNpcDefinitions().getAttackTimer());

		int userHp = user.getStats().getPlayerLevels().get(3);
		if (npc.getDamage() > userHp) {
			npc.setDamage(userHp);
		}
		userHp = userHp - npc.getDamage();
		
		user.getStats().getPlayerLevels().set(3, userHp);
		user.send(new UpdatePvNScreen(this, PvNUpdateType.COMBAT_INFO_NPC));
		user.send(new SetStat(3));
	}
	
	public int calculateMaxHit() {
		return 3;
	}
	
	public void resetCombat(boolean forced, boolean userDead) {
		if (Math.random() * 100 >= 30 || forced || userDead || !userDead) {
			user.setInCombat(false);
			user.setInCombatWithNpc(null);
			user.setLastCombatNpc(npc);
			user.setAttackTimer(0);
			if (forced) {
				user.send(new DrawRoom(user.getRoom()));
			} else {
				//Stuff for victory or loss inhere
				if (userDead) {
					user.initializeDeath();
				} else {
					user.setLoot(null);
					user.setLoot(new NpcDrop(npc).getDrops());
					user.send(new UpdatePvNScreen(this, PvNUpdateType.WIN));
				}
			}
		} else {
			user.send(new MessagePacket("You fail to flee", MessageType.SERVER_MESSAGE));
		}
	}

	public Npc getNpc() {
		return npc;
	}
	
	public User getUser() {
		return user;
	}


}
