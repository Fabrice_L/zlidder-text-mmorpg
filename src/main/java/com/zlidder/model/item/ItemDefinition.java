package com.zlidder.model.item;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class ItemDefinition {

	GameItem gameItem;
	static ArrayList<ItemDefinition> definitions = new ArrayList<ItemDefinition>();
	
	private int id;
	private String name;
	private boolean stackable;
	private String description;
	private ItemType itemType;
	private JSONArray attackAnimations = new JSONArray();
	
	public ItemDefinition(GameItem gameItem) {
		this.gameItem = gameItem;
	}
	
	public String getName() {
		for (ItemDefinition item : definitions) {
			if (item.id == gameItem.getId()) {
				return item.name;
			}
		}
		return null;
	}
	
	public String getDescription() {
		for (ItemDefinition item : definitions) {
			if (item.id == gameItem.getId()) {
				return item.description;
			}
		}
		return null;
	}

	public boolean isStackable() {
		for (ItemDefinition item : definitions) {
			if (item.id == gameItem.getId()) {
				return item.stackable;
			}		
		}
		return false;
	}
	
	public ItemType getItemType() {
		for (ItemDefinition item : definitions) {
			if (item.id == gameItem.getId()) {
				return item.itemType;
			}
		}
		return null;
	}
	
	public JSONArray getAttackAnimations() {
		for (ItemDefinition item : definitions) {
			if (item.id == gameItem.getId()) {
				return item.attackAnimations;
			}
		}
		return null;
	}
	
	public int getAttackTimer() {
		for (ItemDefinition item : definitions) {
			if (item.id == gameItem.getId()) {
				String name = item.name.toLowerCase();
				if (name.endsWith("longsword")) {
					return 4;
				}
				if (name.endsWith("twohanded sword")) {
					return 5;
				}
			}
		}
		return 3;
	}
	
	public static void loadItems(File file) {
		try {
			JSONTokener tokener = new JSONTokener(file.toURI().toURL().openStream());
			JSONObject jsonObject = new JSONObject(tokener);
			JSONArray jsonArray = jsonObject.getJSONArray("items");
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject object = jsonArray.getJSONObject(i);
				ItemDefinition item = new ItemDefinition(null);
				item.id = object.getInt("id");
				item.name = object.getString("name");
				item.stackable = object.getBoolean("stackable");
				item.description = object.getString("description");
				item.itemType = ItemType.getItemType(object.getString("type"));
				item.attackAnimations = object.getJSONArray("attackAnimations");
				definitions.add(item);
			}
			
			System.out.println("loaded " + definitions.size() + " item definitions.");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
