package com.zlidder.model.item;

public enum ItemType {
	HEAD("head"),
	NECK("neck"),
	WEAPON("weapon"),
	QUIVER("quiver"),
	SHIELD("shield"),
	CHESTPLATE("chestplate"),
	CAPE("cape"),
	LEGS("legs"),
	FEET("foot"),
	FOOD("food");
	
	String itemType;
	
	ItemType(String itemType) {
		this.itemType = itemType;
	}
	
	public static ItemType getItemType(String itemType) {
		for (ItemType type : values()) {
			if (type.itemType.equalsIgnoreCase(itemType))
				return type;
		}
		return null;
	}
}
