package com.zlidder.model.item.container;

import com.zlidder.model.MessageType;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.out.MessagePacket;

public abstract class ItemContainer {

	private User user;
	
	private int capacity;
	
	private GameItem[] items;
	
	public ItemContainer(User user, int capacity) {
		this.user = user;
		this.capacity = capacity;
		this.items = new GameItem[capacity];
		
		for (int i = 0; i < capacity; i++) {
			getItems()[i] = new GameItem(0, 0);
		}
	}
	
	public GameItem[] getItems() {
		return items;
	}
	
	public void setItems(GameItem[] items) {
		this.items = items;
	}
	
	public abstract boolean addItem(GameItem item, int slot);
	
	public abstract boolean removeItem(GameItem item, int slot);
	
	public boolean addItem(GameItem item) {
		return addItem(item, -1);
	}
	
	public boolean removeItem(GameItem item) {
		return removeItem(item, -1);
	}
			
	public int getItemSlot(GameItem item) {
		for (int slot = 0; slot < capacity; slot++) {
			if (getItems()[slot].getId() == item.getId()) {
				return slot;
			}
		}
		return -1;	
	}
	
	public boolean containsItem(GameItem item) {
		for (GameItem items : getItems()) {
			if (items.getId() == item.getId()) {
				return true;
			}
		}
		return false;
	}

	public boolean containsItemAmount(GameItem item) {
		for (GameItem items : getItems()) {
			if (items.getId() == item.getId() && items.getAmount() == item.getAmount()) {
				return true;
			}
		}
		getUser().send(new MessagePacket("You don't have enough of this item stored to do this", MessageType.SERVER_MESSAGE));
		return false;
	}
	
	public boolean hasFreeSlots() {
		for(GameItem item : getItems()) {
			if (item.getId() < 1) {
				return true;
			}
		}
		return false;
	}
	
	public int getFreeSlot() {
		for(int slot = 0; slot < capacity; slot++) {
			if (getItems()[slot].getId() < 1) {
				return slot;
			}
		}
		return -1;
	}
	
	public int freeSlotAmount() {
		int freeSlots = 0;
		for (int slot = 0; slot < capacity; slot++) {
			if (getItems()[slot].getId() < 1) {
				freeSlots++;
			}
		}
		return freeSlots;
	}
	
	public int getItemAmount(GameItem item, boolean stacked) {
		int amount = 0;
		for(GameItem items : getItems()) {
			if(items.getId() == item.getId()) {
				if (stacked) {
					return items.getAmount();
				}
				amount++;
			}
		}
		return amount;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public int getCapacity() {
		return capacity;
	}

}
