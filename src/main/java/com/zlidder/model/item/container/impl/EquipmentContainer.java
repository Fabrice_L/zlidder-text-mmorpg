package com.zlidder.model.item.container.impl;

import com.zlidder.model.item.GameItem;
import com.zlidder.model.item.container.ContainerType;
import com.zlidder.model.item.container.ItemContainer;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.out.RefreshItems;

public class EquipmentContainer extends ItemContainer {

	public EquipmentContainer(User user) {
		super(user, ContainerType.EQUIPMENT.getCapacity());
	}

	@Override
	public boolean addItem(GameItem item, int slot) {
		if (containsItem(item) && item.getDefinitions().isStackable()) {
			getItems()[slot].addAmount(item.getAmount());
		} else {
			getItems()[slot] = item;
		}
		getUser().send(new RefreshItems(this, ContainerType.EQUIPMENT));
		return true;
	}

	@Override
	public boolean removeItem(GameItem item, int slot) {
		// TODO Auto-generated method stub
		if (!containsItem(item)) {
			return false;
		}
		getItems()[slot] = new GameItem(0, 0);
		getUser().send(new RefreshItems(this, ContainerType.EQUIPMENT));
		return true;
	}

}
