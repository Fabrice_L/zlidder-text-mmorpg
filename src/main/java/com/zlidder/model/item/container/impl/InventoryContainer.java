package com.zlidder.model.item.container.impl;

import com.zlidder.model.MessageType;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.item.container.ContainerType;
import com.zlidder.model.item.container.ItemContainer;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.out.MessagePacket;
import com.zlidder.net.packet.out.RefreshItems;

public class InventoryContainer extends ItemContainer{

	public InventoryContainer(User user) {
		super(user, ContainerType.INVENTORY.getCapacity());
	}

	@Override
	public boolean addItem(GameItem item, int slot) {
		if(item.getAmount() < 1) {
			return false;
		}
		
		if((!hasFreeSlots() || getFreeSlot() < 0) && !(containsItem(item) && item.getDefinitions().isStackable())) {
			getUser().send(new MessagePacket("There isn't enough storage space to do this!", MessageType.SERVER_MESSAGE));
			return false;
		}
		
		int itemSlot = slot > 0 ? slot : getItemSlot(item);
		
		if(containsItem(item) && item.getDefinitions().isStackable() && itemSlot >= 0) {
			if (getItems()[itemSlot].addAmount(item.getAmount()) < 1) {
				getItems()[itemSlot].setAmount(Integer.MAX_VALUE);
			}
		} else {
			if (item.getAmount() == 1) {
					getItems()[getFreeSlot()] = item;
			} else {
				int amount = item.getAmount();
				if(!item.getDefinitions().isStackable()) {
					while (amount > 0) {
						if (hasFreeSlots()) {
							getItems()[getFreeSlot()] = new GameItem(item.getId(), 1);
							amount--;
						} else {
							amount = 0;
							getUser().send(new MessagePacket("There isn't enough storage space to do this!", MessageType.SERVER_MESSAGE));
						}
					}
				} else {
					if(hasFreeSlots()) {
						getItems()[getFreeSlot()] = item;
					} else {
						getUser().send(new MessagePacket("There isn't enough storage space to do this!", MessageType.SERVER_MESSAGE));
					}
				}
			}
		}
		getUser().send(new RefreshItems(this, ContainerType.INVENTORY));
		return true;
	}

	@Override
	public boolean removeItem(GameItem item, int slot) {
		if (!containsItem(item)) {
			return false;
		}
		
		if (item.getAmount() < 1) {
			return false;
		}
		
		int itemSlot = slot > 0 ? slot : getItemSlot(item);
		if (item.getDefinitions().isStackable()) {
			if (getItems()[itemSlot].getAmount() - item.getAmount() < 1) {
				getItems()[itemSlot] = new GameItem(0, 0);
			} else {
				getItems()[itemSlot].removeAmount(item.getAmount());
			}
		} else {
			if (item.getAmount() == 1) {
				getItems()[itemSlot] = new GameItem(0, 0);
			} else {
				int amount = item.getAmount();
				while (containsItem(item) && amount > 0) {
					getItems()[getItemSlot(item)] = new GameItem(0, 0);
					amount--;
				}
			}
		}
		getUser().send(new RefreshItems(this, ContainerType.INVENTORY));
		return true;
	}

}
