package com.zlidder.model.item.container;
public enum ContainerType {
	INVENTORY(30, "inventory"),
	EQUIPMENT(9, "equipment");
	
	private int capacity;
	private String containerName;
	
	ContainerType(int capacity, String containerName) {
		this.capacity = capacity;
		this.containerName = containerName;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public String getContainerName() {
		return containerName;
	}
	
	public static ContainerType getContainer(String containerName) {
		for (ContainerType type : values()) {
			if (type.getContainerName().equalsIgnoreCase(containerName)) {
				return type;
			}
		}
		return null;
	}
	
}