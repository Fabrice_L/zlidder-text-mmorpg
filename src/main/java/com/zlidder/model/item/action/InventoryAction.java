package com.zlidder.model.item.action;

import com.zlidder.model.MessageType;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.item.ItemType;
import com.zlidder.model.item.equipment.Equipment;
import com.zlidder.model.item.equipment.EquipmentHandler;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.out.MessagePacket;

public class InventoryAction {

	User user;
	GameItem item;
	
	public InventoryAction(User user, int itemIndex) {
		this.user = user;
		this.item = user.getInventoryContainer().getItems()[itemIndex];
	}

	public void doAction() {
		user.setItemUsed(null);
		if (Equipment.isEquipable(item)) {
			new EquipmentHandler(user, item).wearItem();				
		}
		if (item.getDefinitions().getItemType() == ItemType.FOOD) {
			new FoodAction(user, item).doAction();
		}
		System.out.println("You clicked " + item.getDefinitions().getName());
	}

	public void drop() {
		user.setItemUsed(null);
		user.getInventoryContainer().removeItem(item);
		user.send(new MessagePacket("Dropped items can't be recovered!", MessageType.SERVER_MESSAGE));
	}

	public void useWith() {
		if (user.getItemUsed() == null) {
			user.setItemUsed(item);
			user.send(new MessagePacket("Use " + item.getDefinitions().getName() + " with... (CLICK SECOND ITEM)", MessageType.SERVER_MESSAGE));
			return;
		}
		user.setItemUsed(null);
	}

}
