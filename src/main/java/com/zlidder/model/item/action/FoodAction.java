package com.zlidder.model.item.action;

import com.zlidder.model.MessageType;
import com.zlidder.model.item.Food;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.user.Stats;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.out.MessagePacket;
import com.zlidder.net.packet.out.SetStat;

public class FoodAction {

	private User user;
	private GameItem item;
	
	public FoodAction(User user, GameItem item) {
		this.user = user;
		this.item = item;
	}
	
	public void doAction() {
		
		Food food = Food.getFood(item.getId());
		user.send(new MessagePacket("You eat a " + food.getName() + ".", MessageType.SERVER_MESSAGE));
		
		int currentHealth = user.getStats().getPlayerLevels().get(3);
		int maxHealth = Stats.getLevelForExperience(user.getStats().getPlayerExperience().get(3));
		int newHealth = currentHealth + food.getHealthRecovered();
		
		if (newHealth > maxHealth) {
			newHealth = maxHealth;
		}
		
		user.getStats().getPlayerLevels().set(3, newHealth);
		user.send(new SetStat(3));
		user.getInventoryContainer().removeItem(item);
	}
	
}
