package com.zlidder.model.item.action;

import com.zlidder.model.MessageType;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.out.MessagePacket;

public class LootAction {

	private User user;
	private GameItem item;
	private int itemIndex;
	
	public LootAction(User user, int itemIndex) {
		this.user = user;
		this.itemIndex = itemIndex;
		this.item = user.getLoot().get(itemIndex);
	}
	
	public void doAction() {
		if (item == null) {
			return;
		}
		if (user.getLootCollected().contains(itemIndex)) {
			user.send(new MessagePacket("You have collected this already", MessageType.SERVER_MESSAGE));
			return;
		}
		if (user.getInventoryContainer().addItem(item)) {
			//user.getLoot().remove(itemIndex);
			user.getLootCollected().add(itemIndex);
		}
		System.out.println("loot received: " + item.getDefinitions().getName() + ", amount: " + item.getAmount());
	}
	
}
