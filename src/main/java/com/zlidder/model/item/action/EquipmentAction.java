package com.zlidder.model.item.action;

import com.zlidder.model.MessageType;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.out.MessagePacket;

public class EquipmentAction {

	User user;
	GameItem item;
	int itemIndex;
	
	public EquipmentAction(User user, int itemIndex) {
		this.user = user;
		this.item = user.getEquipmentContainer().getItems()[itemIndex];
		this.itemIndex = itemIndex;
	}

	public void doAction() {
		if (user.getInventoryContainer().addItem(item)) {
			user.getEquipmentContainer().removeItem(item, itemIndex);
		} else {
			user.send(new MessagePacket("Didn't work", MessageType.SERVER_MESSAGE));
		}
	}

}
