package com.zlidder.model.item;

public enum Food {

	SARDINES(9, 3, "sardine");
	
	int id;
	int healthRecovered;
	String name;
	
	Food(int id, int healthRecovered, String name) {
		this.id = id;
		this.healthRecovered = healthRecovered;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public int getHealthRecovered() {
		return healthRecovered;
	}
	
	public String getName() {
		return name;
	}
	
	public static Food getFood(int foodId) {
		for (Food food : values()) {
			if (food.id == foodId)
				return food;
		}
		return null;
	}
	
}
