package com.zlidder.model.item;

public class GameItem {

	private int id;
	private int amount;
	private ItemDefinition itemDefinition = new ItemDefinition(this);
	
	public GameItem(int id, int amount) {
		if (amount < 0) {
			throw new IllegalArgumentException("Count cannot be negative.");
		}
		this.setId(id);
		this.setAmount(amount);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public int addAmount(int amount) {
		return this.amount += amount;
	}
	
	public int removeAmount(int amount) {
		return this.amount -= amount;
	}
	
	public ItemDefinition getDefinitions() {
		return itemDefinition;
	}
	
}
