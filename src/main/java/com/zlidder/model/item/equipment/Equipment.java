package com.zlidder.model.item.equipment;

import com.zlidder.model.item.GameItem;
import com.zlidder.model.item.ItemType;

public enum Equipment {
		HEAD(0, ItemType.HEAD),
		NECK(1, ItemType.NECK),
		WEAPON(2, ItemType.WEAPON),
		QUIVER(3, ItemType.QUIVER),
		SHIELD(4, ItemType.SHIELD),
		CHESTPLATE(5, ItemType.CHESTPLATE),
		CAPE(6, ItemType.CAPE),
		LEGS(7, ItemType.LEGS),
		FEET(8, ItemType.FEET);

	  private int slot;
	  private ItemType itemType;

	  Equipment(int slot, ItemType itemType) {
	    this.slot = slot;
	    this.itemType = itemType;
	  }

	  public int getSlot() {
		  return slot;
	  }
	  
	  public ItemType getItemType() {
		  return itemType;
	  }

	  
	  public static boolean isEquipable(GameItem item) {
		  for (Equipment equipment : values()) {
			  if (equipment.getItemType() == item.getDefinitions().getItemType()) {
				  return true;
			  }
		  }
		return false;  
	  }
	  
	  public static int getEquipmentSlot(GameItem item) {
		  for (Equipment equipment : values()) {
			  if (equipment.getItemType() == item.getDefinitions().getItemType()) {
				  return equipment.getSlot();
			  }
		  }
		  return -1;
	  }
	  
}
