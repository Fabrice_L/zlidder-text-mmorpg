package com.zlidder.model.item.equipment;

import com.zlidder.model.item.GameItem;
import com.zlidder.model.user.User;

public class EquipmentHandler {

	User user;
	GameItem item;
	GameItem newItem;
	int itemIndex;
	
	public EquipmentHandler(User user, GameItem item) {
		this.user = user;
		this.item = item;
		System.out.println("items : " + item.getAmount());
	}
	
	public void wearItem() {
		if (user.getEquipmentContainer().getItems()[Equipment.getEquipmentSlot(item)].getId() > 0) {
			user.getInventoryContainer().addItem(user.getEquipmentContainer().getItems()[Equipment.getEquipmentSlot(item)]);
		}
		user.getEquipmentContainer().addItem(item, Equipment.getEquipmentSlot(item));
		user.getInventoryContainer().removeItem(item);
	}

}
