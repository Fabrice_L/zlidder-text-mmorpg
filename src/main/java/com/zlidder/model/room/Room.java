package com.zlidder.model.room;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Room {

	private String locationName;
	private String roomName;
	private int innerId;
	private String description;
	private Room north;
	private Room east;
	private Room south;
	private Room west;
	private Room northEast;
	private Room northWest;
	private Room southEast;
	private Room southWest;
	
	private JSONArray staticNpcList;
	private JSONArray staticObjectList;
	
	public Room(String locationName, int innerId) {
		this.locationName = locationName;
		this.innerId = innerId;
	}

	public Room loadRoom() {
		
		try {
			File file = new File("./data/rooms/" + locationName + ".json");
			if (!file.exists()) {
				return null;
			}
			JSONTokener tokener = new JSONTokener(file.toURI().toURL().openStream());
			JSONObject jsonObject = new JSONObject(tokener);
			JSONArray jsonArray = jsonObject.getJSONArray("innerRooms");
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject object = jsonArray.getJSONObject(i);
				if (object.getInt("id") == innerId) {
					this.setLocationName(locationName);
					this.setRoomName(object.getString("name"));
					this.setDescription(object.getString("description"));
					
					this.setStaticNpcList(object.has("static-npcs") ? object.getJSONArray("static-npcs") : null);
					
					JSONArray northRoom = object.has("north") ? object.getJSONArray("north") : null;
					JSONArray eastRoom = object.has("east") ? object.getJSONArray("east") : null;
					JSONArray southRoom = object.has("south") ? object.getJSONArray("south") : null;	
					JSONArray westRoom = object.has("west") ? object.getJSONArray("west") : null;	
					JSONArray northEastRoom = object.has("northEast") ? object.getJSONArray("northEast") : null;	
					JSONArray northWestRoom = object.has("northWest") ? object.getJSONArray("northWest") : null;	
					JSONArray southEastRoom = object.has("southEast") ? object.getJSONArray("southEast") : null;	
					JSONArray southWestRoom = object.has("southWest") ? object.getJSONArray("southWest") : null;	
		
					
					if (northRoom != null) {
						this.setNorth(new Room(northRoom.getString(0), northRoom.getInt(1)));
					}
					if (eastRoom != null) {
						this.setEast(new Room(eastRoom.getString(0), eastRoom.getInt(1)));
					}
					if (southRoom != null) {
						this.setSouth(new Room(southRoom.getString(0), southRoom.getInt(1)));
					}
					if (westRoom != null) {
						this.setWest(new Room(westRoom.getString(0), westRoom.getInt(1)));
					}
					if (northEastRoom != null) {
						this.setNorthEast(new Room(northEastRoom.getString(0), northEastRoom.getInt(1)));
					}
					if (northWestRoom != null) {
						this.setNorthWest(new Room(northWestRoom.getString(0), northWestRoom.getInt(1)));
					}
					if (southEastRoom != null) {
						this.setSouthEast(new Room(southEastRoom.getString(0), southEastRoom.getInt(1)));
					}
					if (southWestRoom != null) {
						this.setSouthWest(new Room(southWestRoom.getString(0), southWestRoom.getInt(1)));
					}
					
					return this;
				}
			}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getRoomName() {
		return roomName;
	}
	
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Room getNorth() {
		return north;
	}

	public void setNorth(Room north) {
		this.north = north;
	}

	public Room getEast() {
		return east;
	}

	public void setEast(Room east) {
		this.east = east;
	}

	public Room getSouth() {
		return south;
	}

	public void setSouth(Room south) {
		this.south = south;
	}

	public Room getWest() {
		return west;
	}

	public void setWest(Room west) {
		this.west = west;
	}

	public Room getNorthEast() {
		return northEast;
	}

	public void setNorthEast(Room northEast) {
		this.northEast = northEast;
	}

	public Room getNorthWest() {
		return northWest;
	}

	public void setNorthWest(Room northWest) {
		this.northWest = northWest;
	}

	public Room getSouthEast() {
		return southEast;
	}

	public void setSouthEast(Room southEast) {
		this.southEast = southEast;
	}

	public Room getSouthWest() {
		return southWest;
	}

	public void setSouthWest(Room southWest) {
		this.southWest = southWest;
	}

	public JSONArray getStaticNpcList() {
		return staticNpcList;
	}

	public void setStaticNpcList(JSONArray staticNpcList) {
		this.staticNpcList = staticNpcList;
	}

	public JSONArray getStaticObjectList() {
		return staticObjectList;
	}

	public void setStaticObjectList(JSONArray staticObjectList) {
		this.staticObjectList = staticObjectList;
	}
}
