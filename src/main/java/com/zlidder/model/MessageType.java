package com.zlidder.model;

public enum MessageType {
	CHAT_MESSAGE,
	SERVER_MESSAGE,
	GLOBAL_MESSAGE,
}
