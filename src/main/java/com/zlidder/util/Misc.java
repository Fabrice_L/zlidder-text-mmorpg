package com.zlidder.util;

import java.util.concurrent.ThreadLocalRandom;

public class Misc {
	
	public static int random(int range) {
		return (int)(java.lang.Math.random() * (range+1));
	}
	
	public static int random(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}
}
