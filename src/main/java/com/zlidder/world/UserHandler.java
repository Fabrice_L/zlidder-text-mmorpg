package com.zlidder.world;

import java.util.ArrayList;

import org.eclipse.jetty.websocket.api.Session;

import com.zlidder.model.user.User;

public class UserHandler {
	
    private static final ArrayList<User> users = new ArrayList<>();
    private static final ArrayList<User> connections = new ArrayList<>();

	public static ArrayList<User> getUsers() {
		return users;
	}
	
	public static User findConnection(Session session) {
		for (User connection : getConnections()) {
			if (connection.getSession() == session) {
				return connection;
			}
		}
		return null;	
	}
	
	public static boolean isOnline(String username) {
		for (User users : getUsers()) {
			if (users.getUsername() != null) {
				if (users.getUsername().equalsIgnoreCase(username) && users.isOnline()) {
					return true;
				}
			}
		}
		return false;
	}

	public static ArrayList<User> getConnections() {
		return connections;
	}

}
