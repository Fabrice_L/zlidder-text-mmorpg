package com.zlidder.net;
import java.io.IOException;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.json.JSONArray;

import com.zlidder.model.user.User;
import com.zlidder.world.UserHandler;

@WebSocket(maxIdleTime=10000000)
public class WebSocketHandler {
	
    @OnWebSocketConnect
    public void connected(Session session) {
    	System.out.println("connection received from: " + session.getRemoteAddress());
    	new User(session);
    }

    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason) {
        User user = UserHandler.findConnection(session);
        user.destroy();
    }

    @OnWebSocketMessage
    public void message(Session session, String message) throws IOException {
        JSONArray packetArray = new JSONArray(message);
        User user = UserHandler.findConnection(session);
        PacketHandler.parseIncomingPacket(user, packetArray);
    }
    
}