package com.zlidder.net.packet.out;

import java.io.IOException;

import org.json.JSONArray;

import com.zlidder.model.combat.PvN;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.item.equipment.Equipment;
import com.zlidder.model.npc.Npc;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Misc;

public class UpdatePvNScreen extends OutgoingPacket {

	private PvN pvn;
	private PvNUpdateType updateType;
	
	public UpdatePvNScreen(PvN pvn, PvNUpdateType updateType) {
		super(9);
		this.pvn = pvn;
		this.updateType = updateType;

	}

	@Override
	public void send(User user) {
		JSONArray packetData = new JSONArray();
		packetData.put(getOpcode());
		packetData.put(updateType);
		
		if (updateType == PvNUpdateType.NPC_INFO) {
			Npc npc = pvn.getNpc();
			packetData.put(npc.getCurrentHitpoints());
			packetData.put(npc.getNpcDefinitions().getHitpoints());
		}
		
		if (updateType == PvNUpdateType.COMBAT_INFO_NPC) {
			Npc npc = pvn.getNpc();
			int randomAnim = 0;

			if (npc.getDamage() > 0 && npc.getDamage() <= 10) {
				randomAnim = Misc.random(1, 2);
			}
			if (npc.getDamage() > 10 && npc.getDamage() <= 20 ) {
				randomAnim = Misc.random(2, 4);
			}
			if (npc.getDamage() > 20){
				randomAnim = Misc.random(3, 5);
			}
			String npcAnim = npc.getNpcDefinitions().getAnimations().getString(randomAnim);
			packetData.put(npc.getDamage());
			packetData.put(npcAnim);
		}
		
		if (updateType == PvNUpdateType.COMBAT_INFO_USER) {
			int randomAnim = 0;
			if (user.getDamage() > 0 && user.getDamage() <= 10) {
				randomAnim = Misc.random(1, 2);
			}
			if (user.getDamage() > 10 && user.getDamage() <= 20 ) {
				randomAnim = Misc.random(2, 4);
			}
			if (user.getDamage() > 20){
				randomAnim = Misc.random(3, 5);
			}
			GameItem item = user.getEquipmentContainer().getItems()[Equipment.WEAPON.getSlot()];
			String anim;
			if (item.getId() > 0) {
				anim = item.getDefinitions().getAttackAnimations().getString(randomAnim);
			} else {
				anim = "fist flies towards your enemy";
			}
			
			packetData.put(user.getDamage());
			packetData.put(anim);
		}
		
		if (updateType == PvNUpdateType.WIN) {
			for (GameItem item : user.getLoot()) {
				int itemAmount = item.getAmount();
				String itemName = item.getDefinitions().getName();
				packetData.put(new JSONArray().put(itemName).put(itemAmount));
			}
		}

		try {
			user.getSession().getRemote().sendString(packetData.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public enum PvNUpdateType {
		NPC_INFO,
		COMBAT_BUTTONS,
		COMBAT_INFO_NPC,
		COMBAT_INFO_USER,
		WIN
	}

}
