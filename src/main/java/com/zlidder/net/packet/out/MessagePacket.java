package com.zlidder.net.packet.out;

import java.io.IOException;

import org.json.JSONArray;

import com.zlidder.model.MessageType;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.world.UserHandler;

public class MessagePacket extends OutgoingPacket {

	String message;
	MessageType type;
	
	public MessagePacket(String message, MessageType type) {
		super(3);
		this.message = message.replaceAll("<", "&lt").replaceAll(">", "&gt");
		this.type = type;
	}

	@Override
	public void send(User user) {
		JSONArray packetData = new JSONArray();
		packetData.put(getOpcode());
		
		if (type == MessageType.CHAT_MESSAGE) {
			sendRegularChat(user, packetData);
		}
		
		if (type == MessageType.SERVER_MESSAGE) {
			sendServerMessage(user, packetData);
		}
	}

	private void sendServerMessage(User user, JSONArray packetData) {
		packetData.put(type);
		packetData.put(message);
		try {
			user.getSession().getRemote().sendString(packetData.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void sendRegularChat(User user, JSONArray packetData) {
		packetData.put(type);
		packetData.put(user.getUsername());
		packetData.put(message);
		try {
			for (User users : UserHandler.getUsers()) {
				users.getSession().getRemote().sendString(packetData.toString());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

}
