package com.zlidder.net.packet.out;

import org.json.JSONArray;

import com.zlidder.model.user.User;
import com.zlidder.net.packet.OutgoingPacket;

public class LoginResponsePacket extends OutgoingPacket {
	
	private int returnCode;
	
	public LoginResponsePacket(int returnCode) {
		super(1);
		this.returnCode = returnCode;
	}

	@Override
	public void send(User user) {
		JSONArray packetData = new JSONArray();
		packetData.put(getOpcode());
		packetData.put(returnCode);
		try {
			user.getSession().getRemote().sendString(packetData.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (returnCode == 2){	
			user.initialize();
		}
		
	}

}
