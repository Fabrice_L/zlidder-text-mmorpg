package com.zlidder.net.packet.out;

import java.io.IOException;

import org.json.JSONArray;

import com.zlidder.model.npc.Npc;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.OutgoingPacket;

public class DrawPvNScreen extends OutgoingPacket {

	private Npc npc;
	
	public DrawPvNScreen(Npc npc) {
		super(8);
		this.npc = npc;

	}

	@Override
	public void send(User user) {
		JSONArray packetData = new JSONArray();
		packetData.put(getOpcode());
		packetData.put(user.getUsername());
		packetData.put(npc.getNpcDefinitions().getName());
		packetData.put(npc.getNpcDefinitions().getLevel());
		packetData.put(npc.getNpcDefinitions().getAttackStyles());
		packetData.put(npc.getCurrentHitpoints());
		packetData.put(npc.getNpcDefinitions().getHitpoints());
		
		try {
			user.getSession().getRemote().sendString(packetData.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
