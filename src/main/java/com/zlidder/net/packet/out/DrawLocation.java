package com.zlidder.net.packet.out;

import java.io.IOException;

import org.json.JSONArray;

import com.zlidder.model.room.Room;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.OutgoingPacket;

public class DrawLocation extends OutgoingPacket {

	Room room;
	
	public DrawLocation(Room room) {
		super(7);
		this.room = room;
	}

	@Override
	public void send(User user) {
		JSONArray packetData = new JSONArray();
		packetData.put(getOpcode());
		if (room.getNorth() != null) {
			packetData.put(new JSONArray().put("North").put(room.getNorth().loadRoom().getRoomName()));
		}
		if (room.getEast() != null) {
			packetData.put(new JSONArray().put("East").put(room.getEast().loadRoom().getRoomName()));
		}
		if (room.getSouth() != null) {
			packetData.put(new JSONArray().put("South").put(room.getSouth().loadRoom().getRoomName()));
		}
		if (room.getWest() != null) {
			packetData.put(new JSONArray().put("West").put(room.getWest().loadRoom().getRoomName()));
		}
		if (room.getNorthEast() != null) {
			packetData.put(new JSONArray().put("NorthEast").put(room.getNorthEast().loadRoom().getRoomName()));
		}
		if (room.getNorthWest() != null) {
			packetData.put(new JSONArray().put("NorthWest").put(room.getNorthWest().loadRoom().getRoomName()));
		}
		if (room.getSouthEast() != null) {
			packetData.put(new JSONArray().put("SouthEast").put(room.getSouthEast().loadRoom().getRoomName()));
		}
		if (room.getSouthWest() != null) {
			packetData.put(new JSONArray().put("SouthWest").put(room.getSouthWest().loadRoom().getRoomName()));
		}
		
		try {
			user.getSession().getRemote().sendString(packetData.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
