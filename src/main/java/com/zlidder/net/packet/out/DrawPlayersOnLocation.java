package com.zlidder.net.packet.out;

import java.io.IOException;

import org.json.JSONArray;

import com.zlidder.model.user.User;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.world.UserHandler;

public class DrawPlayersOnLocation extends OutgoingPacket {

	JSONArray playersOnline = new JSONArray();
	
	public DrawPlayersOnLocation(User user) {
		super(2);
		for(User users : UserHandler.getUsers()) {
			if (users.getUsername() != null && users.isOnline()
					&& users.getRoom().getLocationName().equalsIgnoreCase(user.getRoom().getLocationName())) {
				playersOnline.put(users.getUsername());
			}
		}
	}

	@Override
	public void send(User user) {

		JSONArray packetData = new JSONArray();
		packetData.put(getOpcode());
		packetData.put(playersOnline);
		System.out.println("user: " + user.getUsername());
		try {
			for(User users : UserHandler.getUsers()) {
				if (users.getRoom().getLocationName().equalsIgnoreCase(user.getRoom().getLocationName())) {
					users.getSession().getRemote().sendString(packetData.toString());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
