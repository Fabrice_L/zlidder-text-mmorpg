package com.zlidder.net.packet.out;

import java.io.IOException;

import org.json.JSONArray;

import com.zlidder.model.npc.Npc;
import com.zlidder.model.room.Room;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.OutgoingPacket;

public class DrawRoom extends OutgoingPacket {

	Room room;
	
	public DrawRoom(Room room) {
		super(6);
		this.room = room;
	}

	@Override
	public void send(User user) {
		JSONArray packetData = new JSONArray();
		JSONArray npcList = new JSONArray(); 
		JSONArray objectList = new JSONArray();
		packetData.put(getOpcode());
		packetData.put(room.getLocationName());
		packetData.put(room.getRoomName());
		packetData.put(room.getDescription());
		
		if (room.getStaticNpcList() != null) {
			for (int npc = 0; npc < room.getStaticNpcList().length(); npc++) {
				Npc npcs = new Npc(room.getStaticNpcList().getInt(npc));
				npcList.put(new JSONArray().put(npcs.getNpcDefinitions().getName()).put(npcs.getNpcDefinitions().getCommands()));
			}
		}
		packetData.put(npcList);
		
		if (room.getStaticObjectList() != null) {
			for (int object = 0; object < room.getStaticObjectList().length(); object++) {
			}
		}
		objectList.put(new JSONArray().put("Tree").put(new JSONArray().put("Cut").put("Test").put("burn")));
		packetData.put(objectList);

		try {
			System.out.println(packetData.toString());
			user.getSession().getRemote().sendString(packetData.toString());
			user.send(new DrawLocation(room));
			user.send(new DrawPlayersOnLocation(user));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
