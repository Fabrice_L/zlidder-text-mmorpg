package com.zlidder.net.packet.out;

import java.io.IOException;

import org.json.JSONArray;

import com.zlidder.model.item.GameItem;
import com.zlidder.model.item.container.ContainerType;
import com.zlidder.model.item.container.ItemContainer;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.OutgoingPacket;

public class RefreshItems extends OutgoingPacket {
	
	ItemContainer itemContainer;
	ContainerType containerType;

	public RefreshItems(ItemContainer itemContainer, ContainerType containerType) {
		super(5);
		this.itemContainer = itemContainer;
		this.containerType = containerType;
	}

	@Override
	public void send(User user) {
		JSONArray packetData = new JSONArray();
		packetData.put(getOpcode());
		packetData.put(containerType);
		for (int i = 0; i < itemContainer.getCapacity(); i++) {
			GameItem item = itemContainer.getItems()[i];
			int itemAmount = item.getAmount();
			String itemName = item.getDefinitions().getName();
			packetData.put(new JSONArray().put(itemName).put(itemAmount));
		}
		
		try {
			user.getSession().getRemote().sendString(packetData.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
