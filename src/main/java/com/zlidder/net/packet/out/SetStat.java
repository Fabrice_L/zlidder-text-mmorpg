package com.zlidder.net.packet.out;

import java.io.IOException;

import org.json.JSONArray;

import com.zlidder.model.user.Stats;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.OutgoingPacket;

public class SetStat extends OutgoingPacket {
	
	int statId;

	public SetStat(int statId) {
		super(4);
		this.statId = statId;
	}

	@Override
	public void send(User user) {
		int level = user.getStats().getPlayerLevels().get(statId);
		int experience = user.getStats().getPlayerExperience().get(statId);
		int maxLevel = Stats.getLevelForExperience(experience);
		JSONArray packetData = new JSONArray();
		packetData.put(getOpcode());
		packetData.put(statId);
		packetData.put(level);
		packetData.put(maxLevel);
		packetData.put(experience);
		try {
			user.getSession().getRemote().sendString(packetData.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
