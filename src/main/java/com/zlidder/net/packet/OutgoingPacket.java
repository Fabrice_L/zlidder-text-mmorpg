package com.zlidder.net.packet;

import com.zlidder.model.user.User;

public abstract class OutgoingPacket {

	private final int opcode;

	public abstract void send(User user);

	public OutgoingPacket(int opcode) {
		this.opcode = opcode;
	}

	public final int getOpcode() {
		return opcode;
	}
}