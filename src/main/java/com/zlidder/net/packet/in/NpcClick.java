package com.zlidder.net.packet.in;

import org.json.JSONArray;

import com.zlidder.model.MessageType;
import com.zlidder.model.combat.PvN;
import com.zlidder.model.npc.Npc;
import com.zlidder.model.user.User;
import com.zlidder.net.PacketHandler;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.net.packet.out.MessagePacket;

public class NpcClick extends IncomingPacket {

	@Override
	public void send(User user, JSONArray packetData) {
		int npcIndex = packetData.getInt(0);
		int npcCommandId = packetData.getInt(1);
		int npcId = user.getRoom().getStaticNpcList().getInt(npcIndex);
		Npc npc = new Npc(npcId);
		String npcCommand = npc.getNpcDefinitions().getCommands().getString(npcCommandId);
		
		if (!PacketHandler.packetValid(user, 1000)) {
			return;
		}
		
		user.send(new MessagePacket("Npc Click initialized: " + npc.getNpcDefinitions().getName() + " | " + npcCommand, MessageType.SERVER_MESSAGE));
	
		switch (npcCommand.toLowerCase()) {
		case "attack":
			new PvN(user, npc).setupNpcCombat();
			break;
		}
	}

}
