package com.zlidder.net.packet.in;

import org.json.JSONArray;

import com.zlidder.model.MessageType;
import com.zlidder.model.user.User;
import com.zlidder.model.user.command.Command;
import com.zlidder.net.PacketHandler;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.net.packet.out.MessagePacket;

public class PublicChat extends IncomingPacket {

	@Override
	public void send(User user, JSONArray packetData) {
		
		if (!PacketHandler.packetValid(user, 100)) {
			return;
		}
				
		if (packetData.getString(0).startsWith("::")) {
			new Command(user, packetData.getString(0).replaceFirst("::", ""));
			return;
		}
		user.send(new MessagePacket(packetData.getString(0), MessageType.CHAT_MESSAGE));
	}

}
