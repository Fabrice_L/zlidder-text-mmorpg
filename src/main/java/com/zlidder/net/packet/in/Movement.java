package com.zlidder.net.packet.in;

import org.json.JSONArray;

import com.zlidder.model.MessageType;
import com.zlidder.model.room.Room;
import com.zlidder.model.user.User;
import com.zlidder.net.PacketHandler;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.net.packet.out.DrawRoom;
import com.zlidder.net.packet.out.MessagePacket;

public class Movement extends IncomingPacket {

	@Override
	public void send(User user, JSONArray packetData) {
		Room room = null;
		
		if (!PacketHandler.packetValid(user, 500)) {
			return;
		}
		
		if (user.getInCombatWithNpc() != null) {
			return;
		}
		
		switch (packetData.getString(0)) {
		case "North":
			room = user.getRoom().getNorth();
			break;
		case "East":
			room = user.getRoom().getEast();
			break;
		case "South":
			room = user.getRoom().getSouth();
			break;
		case "West":
			room = user.getRoom().getWest();
			break;
		case "NorthEast":
			room = user.getRoom().getNorthEast();
			break;
		case "NorthWest":
			room = user.getRoom().getNorthWest();
			break;
		case "SouthEast":
			room = user.getRoom().getSouthEast();
			break;
		case "SouthWest":
			room = user.getRoom().getSouthWest();
			break;
		}
		if (room != null) {
			user.setRoom(room.loadRoom());
			user.send(new DrawRoom(user.getRoom()));
		} else {
			user.send(new MessagePacket("You can't go this direction", MessageType.SERVER_MESSAGE));
		}
	}

}
