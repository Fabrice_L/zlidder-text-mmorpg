package com.zlidder.net.packet.in;

import org.json.JSONArray;

import com.zlidder.model.MessageType;
import com.zlidder.model.user.User;
import com.zlidder.net.PacketHandler;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.net.packet.out.MessagePacket;

public class ObjectClick extends IncomingPacket {

	@Override
	public void send(User user, JSONArray packetData) {
		int objectIndex = packetData.getInt(0);
		int objectCommand = packetData.getInt(1);
		//int npcId = user.getRoom().getStaticNpcList().getInt(npcIndex);
		//Npc npc = new Npc(npcId);
		
		if (!PacketHandler.packetValid(user, 1000)) {
			return;
		}
		
		user.send(new MessagePacket("Object Click initialized (Object_Index: " + objectIndex + " || Command_Index: " + objectCommand + ")", MessageType.SERVER_MESSAGE));
	}

}
