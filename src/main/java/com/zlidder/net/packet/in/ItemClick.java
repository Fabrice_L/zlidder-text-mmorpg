package com.zlidder.net.packet.in;

import org.json.JSONArray;

import com.zlidder.model.item.action.EquipmentAction;
import com.zlidder.model.item.action.InventoryAction;
import com.zlidder.model.item.action.LootAction;
import com.zlidder.model.item.container.ContainerType;
import com.zlidder.model.user.User;
import com.zlidder.net.PacketHandler;
import com.zlidder.net.packet.IncomingPacket;

public class ItemClick extends IncomingPacket {

	@Override
	public void send(User user, JSONArray packetData) {
		String itemLocation = packetData.getString(0);
		ContainerType container = ContainerType.getContainer(itemLocation);
		int itemIndex = packetData.getInt(1);
		String action = packetData.isNull(2) ? null : packetData.getString(2);
		
		if (!PacketHandler.packetValid(user, 100)) {
			return;
		}
		
		if (container == ContainerType.INVENTORY) {
			if (action == null || action.equals("USE")) {
				new InventoryAction(user, itemIndex).doAction();
			}
			if (action.equals("DROP")) {
				new InventoryAction(user, itemIndex).drop();
			}
			if (action.equals("USEWITH")) {
				new InventoryAction(user, itemIndex).useWith();
			}
		}
		
		if (container == ContainerType.EQUIPMENT) {
			new EquipmentAction(user, itemIndex).doAction();
		}
		
		if (itemLocation.equals("loot")) {
			new LootAction(user, itemIndex).doAction();
		}
		
		
		System.out.println("Clicked item index " + itemIndex + " at " + itemLocation);
		
		
	}

}
