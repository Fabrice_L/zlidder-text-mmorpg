package com.zlidder.net.packet.in;

import org.json.JSONArray;

import com.zlidder.model.room.Room;
import com.zlidder.model.user.User;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.net.packet.out.LoginResponsePacket;
import com.zlidder.world.UserHandler;

public class LoginPacket extends IncomingPacket {

	User user;
	@Override
	public void send(User user, JSONArray packetData) {
		this.user = user;
		user.send(new LoginResponsePacket(validLogin(packetData)));
	}

	private int validLogin(JSONArray packetData) {
		String username = packetData.getString(0);
		String password = packetData.getString(1);
			
		if (!username.equals(password)) {
			return 3;
		}
		if (UserHandler.isOnline(username)) {
			return 5;
		}
		user.setUsername(username);
		user.setPassword(password);
		user.setOnline(true);
		user.setRoom(new Room("Serpy", 1).loadRoom());
		return 2;
	}

}
