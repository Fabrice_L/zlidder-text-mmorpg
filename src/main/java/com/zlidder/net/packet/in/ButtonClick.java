package com.zlidder.net.packet.in;

import org.json.JSONArray;

import com.zlidder.model.combat.PvN;
import com.zlidder.model.user.User;
import com.zlidder.net.PacketHandler;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.net.packet.out.DrawRoom;

public class ButtonClick extends IncomingPacket {

	@Override
	public void send(User user, JSONArray packetData) {
		String originalString = packetData.getString(0);
		int spaceIdx = originalString.indexOf("_") + 1;
		int button = Integer.parseInt(originalString.substring(spaceIdx));
		
		if (!PacketHandler.packetValid(user, 1000)) {
			return;
		}
		
		if (button == 1) {	
			if (user.getInCombatWithNpc() == null) {
				return;
			}
			PvN pvn = new PvN(user, user.getInCombatWithNpc());
			if (!user.isInCombat()) {
				pvn.startCombat();
			} else {
				pvn.resetCombat(true, false);
			}
		}
		
		if (button == 2) {
			if (user.getLastCombatNpc() == null) {
				return;
			}
			new PvN(user, user.getLastCombatNpc()).setupNpcCombat();
		}
		
		if (button == 3) {
			user.send(new DrawRoom(user.getRoom()));
		}

	}

}
