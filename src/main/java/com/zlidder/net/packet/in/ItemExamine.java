package com.zlidder.net.packet.in;

import org.json.JSONArray;

import com.zlidder.model.MessageType;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.user.User;
import com.zlidder.net.PacketHandler;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.net.packet.out.MessagePacket;

public class ItemExamine extends IncomingPacket {

	@Override
	public void send(User user, JSONArray packetData) {
		System.out.println(packetData);
		GameItem item = user.getInventoryContainer().getItems()[packetData.getInt(0)];
		
		if (!PacketHandler.packetValid(user, 1000)) {
			return;
		}
		
		user.send(new MessagePacket(item.getDefinitions().getDescription() + (item.getAmount() > 1 ? " (" + item.getAmount() + ")" : ""), MessageType.SERVER_MESSAGE));
	}

}
