package com.zlidder.net.packet;

import org.json.JSONArray;

import com.zlidder.model.user.User;
import com.zlidder.net.packet.in.ButtonClick;
import com.zlidder.net.packet.in.ItemClick;
import com.zlidder.net.packet.in.ItemExamine;
import com.zlidder.net.packet.in.LoginPacket;
import com.zlidder.net.packet.in.Movement;
import com.zlidder.net.packet.in.NpcClick;
import com.zlidder.net.packet.in.ObjectClick;
import com.zlidder.net.packet.in.PublicChat;

/**
 * 
 * @author fabrice
 *
 */

public abstract class IncomingPacket {

	private static final IncomingPacket[] packets = new IncomingPacket[255];
	
	public abstract void send(User user, JSONArray packetData);

	public static IncomingPacket[] getPackets() {
		return packets;
	}
	
	static {
		packets[1] = new LoginPacket();
		packets[2] = new PublicChat();
		packets[3] = new ButtonClick();
		packets[4] = new ItemClick();
		packets[5] = new ItemExamine();
		packets[6] = new Movement();
		packets[7] = new NpcClick();
		packets[8] = new ObjectClick();
	}
}