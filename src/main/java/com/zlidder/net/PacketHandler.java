package com.zlidder.net;

import org.json.JSONArray;

import com.zlidder.model.user.User;
import com.zlidder.net.packet.IncomingPacket;

public class PacketHandler {
	
	public static void parseIncomingPacket(User user, JSONArray packetArray) {
		int packetType = (int) packetArray.get(0);
		JSONArray packetData = new JSONArray();
		for (int i = 1; i < packetArray.length(); i++) {
			packetData.put(packetArray.get(i));
		}
		
		System.out.println(packetType + ", " + packetData);
 
		user.receive(IncomingPacket.getPackets()[packetType], packetData);
	}
	
	public static boolean packetValid(User user, int time) {
		if (user.getLastAction() > System.currentTimeMillis() - time) {
			return false;
		}
		user.setLastAction(System.currentTimeMillis());
		return true;
	}
	
}
